"""Package for convenient work with project databases"""
import logging

from .mongo_db_wrapper import check_db_exists as mongodb_check
from .postgre_db_wrapper import check_db_exists as postgre_db_check

# Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

fh = logging.FileHandler('logs/db_logger.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s in %(funcName)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
fh.setFormatter(formatter)

logger.addHandler(fh)


def initialization() -> int:
    """
    Initializes databases if it's necessary.

    :return: 0 - OK;
             -1 - an error has happened.
    """
    mongo_check = mongodb_check()
    postgre_check = postgre_db_check()

    if mongo_check is None or postgre_check is None:
        # An error has happened during connection or initialization
        return -1

    # Dbs have been connected and checked successfully
    return 0


if __name__ == '__main__':
    initialization()
