create table budget
(
    id          serial primary key,
    codename    varchar(255),
    daily_limit numeric(10, 2) DEFAULT 500
);

create table category
(
    codename        varchar(255) primary key,
    name            varchar(255),
    is_base_expense boolean,
    aliases         text
);

create table expenses
(
    id                   serial primary key,
    user_id              int,
    amount               numeric(10, 2) DEFAULT 0,
    created_at           timestamp      DEFAULT
                                            (CURRENT_TIMESTAMP(0) at time zone 'Europe/Moscow')::TIMESTAMP WITH TIME ZONE,
    category_codename    varchar(255),
    raw_text text,

    FOREIGN KEY (category_codename) REFERENCES category (codename)
);

create table debts
(
    id serial primary key,
    borrower_id int,
    debtor_id int,
    amount numeric(10, 2)
);

insert into category (codename, name, is_base_expense, aliases)
values ('products', 'продукты', true, 'еда'),
       ('coffee', 'кофе', true, ''),
       ('dinner', 'обед', true, 'столовая, ланч, бизнес-ланч, бизнес ланч'),
       ('cafe', 'кафе', true, 'ресторан, рест, мак, макдональдс, макдак, kfc, ilpatio, il patio, бк, burger king'),
       ('transport', 'транспорт', false, 'метро, автобус, metro, тройка'),
       ('taxi', 'такси', false, ''),
       ('phone', 'телефон', false, 'связь'),
       ('books', 'книги', false, 'литература, литра, лит-ра'),
       ('internet', 'интернет', false, 'инет'),
       ('subscriptions', 'подписки', false, 'подписка, netflix, кинопоиск, kinopoisk, okko, ivi'),
       ('debt', 'долг', false, ''),
       ('other', 'прочее', false, '');