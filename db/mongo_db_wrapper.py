"""
Working with mongo FinanceBot database
"""
import os
import logging
from contextlib import contextmanager
from typing import List

from pymongo import MongoClient, ASCENDING
from pymongo.database import Database

logger = logging.getLogger(__name__)


@contextmanager
def mongodb_cursor() -> Database:
    client = MongoClient()
    try:
        with client:
            bot_db: Database = client['FinanceBot']
            yield bot_db
    except Exception as exc:
        logger.error(f'mongodb error: {repr(exc)}')


def check_db_exists():
    """
    Database collections configuration: if not exist - initialize.

    :return: 0 - DB collections are fine;
             None - An error has happened.
    """
    with mongodb_cursor() as bot_db:
        collections: List[str] = bot_db.collection_names()

        if 'registration_codes' not in collections:
            _init_db('registration_codes')

        if 'proved_users' not in collections or \
                bot_db['proved_users'].count() == 0:
            _init_db('proved_users')

        return 0


def _init_db(collection_name: str):
    """
    Initializes the collection passed as collection_name

    :param collection_name: collection name in the DB
    """
    with mongodb_cursor() as bot_db:
        if collection_name == 'registration_codes':
            registration_codes_collection = bot_db['registration_codes']

            registration_codes_collection.create_index('auth_code')

        if collection_name == 'proved_users':
            proved_users_collection = bot_db['proved_users']

            maintainer = {
                '_id': 0,
                'user_id': int(os.getenv('MAINTAINER_TG_ID')),
                'username': os.getenv('MAINTAINER_TG_USERNAME')
            }

            proved_users_collection.insert_one(maintainer)
            proved_users_collection.create_index([('user_id', ASCENDING)], unique=True)


def find_document(collection_name: str, elements: dict = None, multiple=False):
    """
    Function to retrieve single or multiple documents from a provided
    Collection using a dictionary containing a document's elements.

    :param collection_name: Mongo db collection
    :param elements: element, whose documents are searched in the collection
    :param multiple: operating mode
    :type multiple: bool

    :return: the first document in the collection
            with these elements(if multiple is off(=False)).
            Otherwise - list of all such documents.
            If an error has happened - None
    """
    with mongodb_cursor() as bot_db:
        collection = bot_db[collection_name]

        if multiple:
            results = collection.find(elements)
            return [r for r in results]

        proved_user = collection.find_one(elements)

        if not proved_user:
            # There is no such user, function returned None
            return {}
        return proved_user
