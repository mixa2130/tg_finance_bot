"""Wrapper for working with postgreSQL database."""
import os
import logging
from pathlib import Path
from contextlib import contextmanager
from typing import List

from psycopg2 import connect
from psycopg2.extensions import connection, quote_ident

logger = logging.getLogger(__name__)


@contextmanager
def transaction() -> connection:
    """
    Database transaction management
    """
    conn = None  # in the case, when connection wasn't established
    try:
        conn = connect(dbname='expenses', user=os.getenv('EXPENSES_DB_USERNAME'),
                       password=os.getenv('EXPENSES_DB_PASSWORD'), host='127.0.0.1')
        yield conn
        conn.commit()
    except Exception as exc:
        if conn is not None:
            # connection with database was established and
            # we failed in an attempt to make a transaction
            conn.rollback()

        logger.error(f'postgre db error: {repr(exc)}')
    finally:
        if conn is not None:
            conn.close()


def _init_db():
    """
    Database initialization
    """
    with open(Path.cwd() / 'db' / 'create_expenses_db.sql', 'r') as sql_file:
        sql_script = sql_file.read()

    with transaction() as conn:
        cursor = conn.cursor()
        cursor.execute(sql_script)


def check_db_exists():
    """
    Checks whether the database is initialized, if not - initialize

    :return: 0 - DB tables are fine
             None - an error has happened
    """
    with transaction() as conn:
        cursor = conn.cursor()
        cursor.execute("select table_name from information_schema.tables "
                       "where table_schema='public'")

        table_exists = cursor.fetchall()

        if not table_exists:
            _init_db()
        return 0


def insert(table_name: str, column_values: dict) -> tuple:
    """
    Wrapper of the standard sql insert method

    :param table_name: table name in db
    :param column_values: set of inserting pairs: "column": "value"
    """
    with transaction() as conn:
        cursor = conn.cursor()

        columns = ', '.join(column_values.keys())
        values = tuple(column_values.values())

        cursor.execute(f"INSERT INTO {quote_ident(table_name, cursor)} ({columns}) "
                       f"VALUES {values} returning id")
        rows = cursor.fetchall()

        return rows[0]


# def delete(table_name: str, del_condition: dict):
#     """
#     Wrapper of the standard sql delete method based on deletion criteria pairs
#
#     :param table_name: table name in db
#     :param del_condition: deletion criteria: "column": "value"
#
#     :return
#     """
#     if len(del_condition) == 0:
#         logger.error('An attempt to delete the table')
#         return
#
#     with transaction() as conn:
#         cursor = conn.cursor()
#
#         columns = list(del_condition.keys())
#         values = list(del_condition.values())
#
#         stmt = f"DELETE FROM {quote_ident(table_name, cursor)} WHERE " \
#                f"{quote_ident(columns[0], cursor)} = " \
#                f"{quote_ident(values[0], cursor) if isinstance(values[0], str) else values[0]}"
#
#         for index in range(1, len(del_condition)):
#             stmt += f" AND {quote_ident(columns[index], cursor)} = " \
#                     f"{quote_ident(values[index], cursor) if isinstance(values[index], str) else values[index]} "
#
#         stmt += 'returning id'
#         print(stmt)
#         cursor.execute(stmt)
#
#         rows = cursor.fetchall()
#         print(rows)
#         return rows[0]


def fetchall(table_name: str, columns: list = None) -> List[dict]:
    """
    Wrapper of the standard sql select method based on selection criteria pairs

    :param table_name: table name in the db
    :param columns: requested table columns in the db

    :return: If execution happened successfully:
                list of dictionaries - 'column': 'value'
             Else:
                None(returns automatically)
    """
    with transaction() as conn:
        cursor = conn.cursor()

        if columns:
            # Search columns are set
            columns_joined = ', '.join(columns)
        else:
            # We want all data from the table
            columns_joined = '*'
            cursor.execute(f"SELECT COLUMN_NAME FROM information_schema.columns "
                           f"WHERE table_name = '{table_name}'")

            # Cursor returns:
            # [('id',), ('is_base_expense',), ('codename',), ('name',), ('aliases',)]
            columns = [item[0] for item in cursor.fetchall()]

        stmt = f"SELECT {columns_joined} FROM {quote_ident(table_name, cursor)}"

        cursor.execute(stmt)
        rows = cursor.fetchall()

        fetch_res = []
        for row in rows:
            dict_fetch = {}

            for index, value in enumerate(columns):
                dict_fetch[value] = row[index]

            fetch_res.append(dict_fetch)

        return fetch_res
