"""Several auxiliary functions taken out of the project server part"""
import random
from db.mongo_db_wrapper import mongodb_cursor, find_document


def random_element(letters: list, numbers: list):
    """
    Generator for getting random number or letter

    :param letters: letters of the English alphabet
    :param numbers: numbers from 0 to 9
    """
    while True:
        random.shuffle(letters)
        yield random.choice(letters)

        random.shuffle(numbers)
        yield random.choice(numbers)


def create_code():
    """
    Generates a sequence of characters and letters 17 characters long

    :return: sequence of characters and letters 17 characters long
    """
    letters = list(map(chr, range(97, 123)))
    numbers = list(map(chr, range(48, 57)))

    character_gen = random_element(letters, numbers)
    unique_code = ''.join([next(character_gen) for _ in range(17)])

    with mongodb_cursor() as bot_db:
        register_codes_collection = bot_db['registration_codes']
        register_codes_collection.insert_one({'auth_code': unique_code})

    return unique_code


def check_code(message_text: str, user_id: int, username: str) -> int:
    """
    Checks the code for validity

    :param message_text: message with code, sent by user
    :param user_id: tg user id
    :param username: tg username

    :return: -1 - code is incorrect;
            0 - OK;
            None - mongo has failed.
    """
    with mongodb_cursor() as bot_db:
        register_codes_collection = bot_db['registration_codes']
        registration_codes = find_document('registration_codes', multiple=True)

        if message_text not in [registration_codes[i]['auth_code']
                                for i in range(len(registration_codes))]:
            # Code is incorrect
            return -1

        # The code is valid, adding the user to the database
        register_codes_collection.delete_one({'auth_code': message_text})

        proved_users_collection = bot_db['proved_users']
        proved_users_collection.insert_one({
            'user_id': user_id,
            'username': username
        })

        return 0
