"""Working with expense categories"""
from typing import NamedTuple, List

from db.postgre_db_wrapper import fetchall
from exceptions import TransactionFailed


class Category(NamedTuple):
    """Represents category from db"""
    codename: str
    name: str
    is_base_expense: bool
    aliases: List[str]


class Categories:
    def __init__(self):
        self._categories: List[Category] = self._load_categories()

    @staticmethod
    def _load_categories() -> List[Category]:
        """
        Load categories from db

        :return: list of categories
        :raise TransactionFailed: if transaction has failed
        """
        categories = fetchall('category',
                              columns=['codename', 'name', 'is_base_expense', 'aliases'])

        if not categories:
            raise TransactionFailed

        categories_res = []
        for category in categories:
            categories_res.append(Category(
                codename=category['codename'],
                name=category['name'],
                is_base_expense=category['is_base_expense'],
                aliases=category['aliases'].split(',')
            ))

        return categories_res

    def get_category(self, raw_category: str) -> str:
        """
        Looks for a suitable category
        :param raw_category: category text sent by user
        :return: category codename
        """

        for category in self._categories:
            if raw_category in (category.name, category.codename):
                return category.codename

            if len(category.aliases) != 0:
                for alias in category.aliases:
                    if alias == raw_category:
                        return category.codename

        # It's other category
        return 'other'

    def get_all_categories(self) -> List[Category]:
        # Maybe I'll use it one day, just for style
        return self._categories
