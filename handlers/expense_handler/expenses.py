"""Working with expenses"""
import re

from typing import NamedTuple, Tuple, List
from varname import nameof

from exceptions import NotCorrectMessage, TransactionFailed

import db.postgre_db_wrapper as postgre_wrapper
from .categories import Categories


class Message(NamedTuple):
    """Represents a parced message"""
    amount: int
    category_text: str


class Expense(NamedTuple):
    """Represents the result of insertion into the table"""
    ids: tuple
    amount: int
    category_name: str


def _parse_message(message: str) -> Message:
    regexp_result = re.match(r'(\d+[,.]?\d*) (.+)', message)

    if not regexp_result or not regexp_result.group(1) or not regexp_result.group(2):
        raise NotCorrectMessage

    purchase_value = re.split(r'[.,]', regexp_result.group(1))

    amount = int(purchase_value[0])

    if len(purchase_value) == 2:
        # purchase value is fractional

        if int(purchase_value[1][0]) >= 5:
            amount += 1

    return Message(amount=amount, category_text=regexp_result.group(2).strip())


def add_expense(raw_message: str, user_id: str) -> Expense:
    """
    Adds an expense to the table

    :param raw_message: text sent by user
    :param user_id: user id in tg

    :return: result of insertion in a structured format
    :raise TransactionFailed if transaction has failed
    """
    parsed_message: Message = _parse_message(raw_message)
    category_codename = Categories().get_category(parsed_message.category_text)

    insert_id = postgre_wrapper.insert('expenses', {
        nameof(user_id): user_id,
        'amount': parsed_message.amount,
        nameof(category_codename): category_codename,
        'raw_text': raw_message
    })

    if insert_id:
        return Expense(
            ids=insert_id,
            amount=parsed_message.amount,
            category_name=category_codename
        )

    # Transaction has failed
    raise TransactionFailed


def delete_expense(del_exp: str, user_id: str):
    """
    Deletes the expense by its id and user id

    :param del_exp: message about deletion
    :param user_id: user tg id

    :return: id of deletion row;
             else - None.
    :raise TransactionFailed: if an error has happened during transaction
    """
    split_exp: List[str] = del_exp.split(' ')

    with postgre_wrapper.transaction() as conn:
        cursor = conn.cursor()
        cursor.execute(f"DELETE FROM expenses WHERE id = {split_exp[1]} AND "
                       f"user_id = '{user_id}' "
                       f"returning id")

        del_id: tuple = cursor.fetchone()

        if del_id:
            return del_id[0]
        # Row with such expense doesn't exist
        return None

    # Transaction has failed
    raise TransactionFailed


def last(user_id: str) -> str:
    """
    Returns last user expenses

    :param user_id: user tg id

    :return: last
    :raise TransactionFailed: if an error has happened during transaction
    """
    with postgre_wrapper.transaction() as conn:
        cursor = conn.cursor()

        cursor.execute(f"SELECT id, amount, raw_text FROM expenses "
                       f"WHERE user_id='{user_id}' "
                       f"order by id desc limit 5")

        rows: List[tuple] = cursor.fetchall()
        if len(rows) == 0:
            return 'У вас нет последних расходов'

        expenses = 'Последние расходы:\n\n'
        for row in rows:
            expenses += f"id: {row[0]}\n" \
                        f"сумма: {row[1]}\n" \
                        f"Текст сообщения: {row[2]}\n\n"

        expenses += 'Для удаления используйте команду /del и id из сообщения выше.\n' \
                    'Например: /del 1'
        return expenses

    # Transaction has failed
    raise TransactionFailed


def get_today_statistics(user_id: str) -> str:
    """
    Gets spending statistics for a current day

    :param user_id: user tg id

    :return: expense statistics
    :raise TransactionFailed: if an error has happened during transaction
    """
    with postgre_wrapper.transaction() as conn:
        cursor = conn.cursor()

        cursor.execute("SELECT sum(amount) FROM expenses "
                       "WHERE created_at::date = 'today'::date AND "
                       f"user_id = '{user_id}'")

        exec_res: Tuple[tuple] = cursor.fetchone()
        if exec_res[0]:
            total_amount = exec_res[0]
        else:
            return 'Сегодня ещё нет расходов'

        # Base expenses
        cursor.execute(f"SELECT sum(amount) FROM expenses "
                       f"WHERE created_at::date = 'today'::date AND "
                       f"user_id = '{user_id}' AND "
                       f"category_codename in "
                       f"(SELECT codename FROM category WHERE is_base_expense = true)")

        exec_res: Tuple[tuple] = cursor.fetchone()
        basic_expenses_amount = exec_res[0] if exec_res[0] else 0

        return (f"Статистика расходов за сегодня:\n"
                f"Всего - {total_amount} руб.\n"
                f"Базовые - {basic_expenses_amount} руб."
                )

    # Transaction has failed
    raise TransactionFailed
