"""
Access modifiers for handlers
"""
import os

from db.mongo_db_wrapper import find_document


def auth(register_mode=False):
    """
    A decorator with modes that restricts the
    function's availability to certain groups of people.

    If register_mode is off(=False) - decorator prevents access
    by unregistered users.

    Otherwise - prevents access by already registered users.

    :param register_mode: decorator's operation mode
    :type register_mode: bool
    """

    def actual_decorator(func):
        async def wrapper(message):
            proved_users = find_document('proved_users', {
                'user_id': message['from']['id']
            })

            if proved_users is None:
                # Something bad has happened with mongodb
                return await message.answer('База данных не отвечает, обратитесь к администратору!')

            if not register_mode and len(proved_users) == 0:
                # decorator prevents access by unregistered users

                return await message.answer('Похоже что вас нет в базе, используйте '
                                            '/authorize для регистрации!')

            if register_mode and len(proved_users) != 0:
                # decorator prevents access by already registered users

                return await message.answer('Похоже что вы уже зарегистрированы. '
                                            'Используйте комманду /help для навигации')

            return await func(message)

        return wrapper

    return actual_decorator


def auth_maintainer(func):
    """
    A decorator that prevents access by everyone except the Maintainer

    :return: 'Access Denied!' if the message is not from Maintainer
    """

    async def wrapper(message):
        if str(message['from']['id']) != os.getenv('MAINTAINER_TG_ID'):
            return await message.reply('Access Denied!', reply=False)
        return await func(message)

    return wrapper
