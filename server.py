"""Telegram bot server, run directly"""
import logging
import os
import sys
from pathlib import Path
from dotenv import load_dotenv

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import BadRequest

from db import initialization
from exceptions import NotCorrectMessage, TransactionFailed

from middlewares import auth, auth_maintainer
from handlers.expense_handler import expenses
from handlers.auxiliary_functions import create_code, check_code


load_dotenv()

API_TOKEN = os.getenv('TG_BOT_API_TOKEN')

# Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

fh = logging.FileHandler('logs/bot_logger.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.info('Bot started')

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot=bot, storage=MemoryStorage())

# Db initialization
if initialization() == -1:
    logger.error("Bot ended: can't connect to the dbs")
    sys.exit()


# Bot commands
class Registration(StatesGroup):
    """
    User Registration state storage
    """
    waiting_for_registration_code = State()


@dp.message_handler(commands='authorize', state="*")
@auth(True)
async def registration(message: types.Message):
    """
    Starting the registration FSM(Finite State Machine)
    """
    await message.answer('Ваш одноразовый регистрационный код:')
    await Registration.waiting_for_registration_code.set()


@dp.message_handler(state=Registration.waiting_for_registration_code,
                    content_types=types.ContentTypes.TEXT)
async def registration_code_check(message: types.Message, state: FSMContext):
    """
    Getting the registration code, it's verification and, if successful,
    entering the user in the database
    """
    check_res = check_code(message.text, message['from']['id'], message['from']['first_name'])

    if check_res is None:
        # Something bad has happened with mongodb
        await state.finish()
        return await message.answer('База данных не отвечает, обратитесь к администратору!')

    if check_res == -1:
        await message.reply('Неверный код!')
        return  # to stop moving and restart fsm

    # Ok, finish conversation
    await state.finish()

    await message.answer('Вы зарегистрированы, доступ открыт!')
    return await send_welcome(message)


@dp.message_handler(commands=['start', 'help'])
@auth()
async def send_welcome(message: types.Message):
    """
    Bot welcome function
    """
    await message.answer(
        "Бот для учёта финансов\n\n"
        "Добавить расход: 169 подписка\n"
        "Сегодняшняя статистика: /today\n"
        "За текущий месяц: /month\n"
        "Последние внесённые расходы: /expenses\n"
        "Категории трат: /categories")


@dp.message_handler(commands=['create_code'])
@auth_maintainer
async def create_register_code(message: types.Message):
    """
    Generates a sequence of characters and letters 17 characters long
    """
    await message.answer(create_code())


@dp.message_handler(commands=['bot_logs'])
@auth_maintainer
async def bot_logs(message: types.Message):
    """
    Returns bot logs
    """
    with open(Path.cwd() / 'logs/bot_logger.log', 'rb') as file:
        await message.answer_document(file)


@dp.message_handler(commands=['db_logs'])
@auth_maintainer
async def db_logs(message: types.Message):
    """
    Returns database logs
    """
    try:
        with open(Path.cwd() / 'logs/db_logger.log', 'rb') as file:
            await message.answer_document(file)
    except BadRequest:
        await message.answer('Файл пуст')


@dp.message_handler(commands=['expenses'])
@auth()
async def expenses_last(message: types.Message):
    """
    Deletes an expense from db
    """
    try:
        last_expenses = expenses.last(message['from']['id'])
    except TransactionFailed:
        await message.answer('Что-то пошло не так, повторите попытку позднее')
    else:
        await message.answer(last_expenses)


@dp.message_handler(commands=['today'])
@auth()
async def today(message: types.Message):
    """
    Returns statistics for a day
    """
    try:
        await message.answer(expenses.get_today_statistics(message['from']['id']))
    except TransactionFailed:
        await message.answer('Что-то пошло не так, повторите попытку позднее')


@dp.message_handler(regexp=r'^/del \d+')
@auth()
async def del_expense(message: types.Message):
    """
    Deletes an expense, which id is written in the message
    """
    try:
        del_id = expenses.delete_expense(message.text, message['from']['id'])

        if del_id:
            await message.reply('Расход удалён')
        else:
            await message.reply('У вас нет такого расхода, или он принадлежит другому пользователю')

    except TransactionFailed:
        await message.answer('Что-то пошло не так, повторите попытку позднее')


@dp.message_handler(regexp=r'\d+ .+')
@auth()
async def add_expense(message: types.Message):
    """
    Adds an expense written in message
    """
    try:
        expense = expenses.add_expense(message.text, message['from']['id'])

    except TransactionFailed:
        await message.answer('Что-то пошло не так, повторите попытку позднее')
    except NotCorrectMessage:
        # Bot response, if command wasn't recognized
        await message.answer('Команда не опознана. '
                             'Используйте /help для навигации')
    else:
        await message.answer(f'Добавлены траты {expense.amount} руб на {expense.category_name}.\n\n'
                             f"{expenses.get_today_statistics(message['from']['id'])}")


@dp.message_handler(content_types=types.ContentTypes.ANY)
@auth()
async def any_other_message(message: types.Message):
    """
    Bot response, if command wasn't recognized
    """
    await message.answer('Команда не опознана. '
                         'Используйте /help для навигации')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
