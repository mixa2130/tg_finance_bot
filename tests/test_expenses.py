import unittest

from handlers.expense_handler.expenses import _parse_message, Message
from exceptions import NotCorrectMessage


class TestExpenseMessage(unittest.TestCase):
    def test_parse_message(self):
        self.assertEqual(_parse_message('169 subscribe'), Message(amount=169, category_text='subscribe'))
        self.assertEqual(_parse_message('169 il 9 patio'), Message(amount=169, category_text='il 9 patio'))
        self.assertEqual(_parse_message('169 78 subscribe'), Message(amount=169, category_text='78 subscribe'))

        self.assertEqual(_parse_message('169.8 подписка '), Message(amount=170, category_text='подписка'))
        self.assertEqual(_parse_message('169.8  подписка'), Message(amount=170, category_text='подписка'))
        self.assertEqual(_parse_message('169,8 подписка '), Message(amount=170, category_text='подписка'))
        self.assertEqual(_parse_message('169.8343435353535553 подписка яндекс'),
                         Message(amount=170, category_text='подписка яндекс'))
        self.assertEqual(_parse_message('169.456 подписка '), Message(amount=169, category_text='подписка'))

        # warnings
        with self.assertRaises(NotCorrectMessage):
            self.assertEqual(_parse_message('169,,78 subscribe'), NotCorrectMessage)
            self.assertEqual(_parse_message('a69,78 subscribe'), NotCorrectMessage)
            self.assertEqual(_parse_message('169@ subscribe'), NotCorrectMessage)


if __name__ == '__main__':
    unittest.main()
